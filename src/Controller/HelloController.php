<?php

namespace App\Controller;

use Doctrine\DBAL\DriverManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HelloController extends AbstractController
{
    /**
     * @Route("/hello", name="hello")
     */
    public function index()
    {
        $this->getStudents();
        $this->getProfessors();
        return $this->render('hello/index.html.twig', [
            'controller_name' => 'HelloController',
        ]);
    }

    public function getStudents() {

        $rep = $this->getDoctrine()->getRepository('examenDB');
        $q = $rep->createQueryBuilder('t')
            ->select('COUNT(first_name)')
            ->from('student')
            ->getQuery();

        return $res = $q->getResults();
    }

    public function getProfessors() {

        $rep = $this->getDoctrine()->getRepository('examenDB');
        $q = $rep->createQueryBuilder('t')
            ->select('COUNT(first_name)')
            ->from('professor')
            ->getQuery();

        return $res = $q->getResults();
    }

}
