<?php

namespace App\Controller;

use App\Entity\Student;
use Omines\DataTablesBundle\Column\TextColumn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Article;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;


class StudentsController extends AbstractController
{
    /**
     * @Route("/students", name="students")
     */
    public function index()
    {
        return $this->render('students/index.html.twig', [
            'controller_name' => 'StudentsController',
        ]);
    }


    /**
     * @Route("/students", name="students")
     */
    public function ajaxAction(Request $request) {
        $students = $this->getDoctrine()
            ->getRepository(Student::class)
            ->findAll();

        if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {
            $jsonData = array();
            $idx = 0;
            foreach($students as $student) {
                $temp = array(
                    'first_name' => $student->getFirstName(),
                    'last_name' => $student->getLastName(),
                    'email' => $student->getEmail(),
                );
                $jsonData[$idx++] = $temp;
            }
            return new JsonResponse($jsonData);
        } else {
            return $this->render('students/index.html.twig');
        }
    }

}
