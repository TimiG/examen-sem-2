<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProfessorsController extends AbstractController
{
    /**
     * @Route("/professors", name="professors")
     */
    public function index()
    {
        return $this->render('professors/index.html.twig', [
            'controller_name' => 'ProfessorsController',
        ]);
    }
}
